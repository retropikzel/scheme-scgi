run-example-scgi-server:
	cd example && \
		gosh -A .. example/server.scm

run-example-http-server:
	mkdir -p log
	lighttpd -D -f lighttpd.conf
