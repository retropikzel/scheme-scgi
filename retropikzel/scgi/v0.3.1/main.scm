;; Copyright 2022- by Joona "Retropikzel" Isoaho <retropikzel@iki.fi>
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU Lesser General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later version.
;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;; See the GNU Lesser General Public License for more details.
;;You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

(define-library
  (retropikzel scgi v0.3.1 main)
  (import (scheme base)
          (scheme write)
          (scheme char)
          (scheme file)
          (scheme process-context)
          (srfi 106))
  (export scgi-start
          scgi-start-debug
          scgi-start-bridged
          scgi-start-debug-bridged
          scgi-add-request-middleware
          scgi-add-response-middleware)
  (begin

    (define debug? #f)
    (define bridged? #f)
    (define request-middleware (list))
    (define response-middleware (list))
    (define encode-replacements
      (list (list " " "%20")
            (list " " "+")
            (list "!" "%21")
            (list "#" "%23")
            (list "$" "%24")
            (list "%" "%25")
            (list "&" "%26")
            (list "'" "%27")
            (list "(" "%28")
            (list ")" "%29")
            (list "*" "%2A")
            (list "+" "%2B")
            (list "," "%2C")
            (list "/" "%2F")
            (list ":" "%3A")
            (list ";" "%3B")
            (list "=" "%3D")
            (list "?" "%3F")
            (list "@" "%40")
            (list "[" "%5B")
            (list "]" "%5D")
            (list "<" "%3C")
            (list ">" "%3E")
            (list "\\" "%5C")
            (list "\"" "%22")
            (list "\n" "%0A")
            (list "\r" "%0D")))

    (define decode-replacements (map reverse encode-replacements))

    (define make-html-error-message
      (lambda (ex request body handler)
        (parameterize ((current-output-port (open-output-string)))
          (display "Content-type: text/html\r\n\r\n")
          (display "<h1>Error</h1>")
          (display "<br/>")
          (display "<h2>Message</h2>")
          (display "<p>")
          (display (error-object-message ex))
          (display "</p>")
          (display "<h2>Irritants</h2>")
          (display "<p>")
          (display (error-object-irritants ex))
          (display "</p>")
          (display "<br/>")
          (display "<h2>Request</h2>")
          (display "<br/>")
          (display "<p>")
          (display request)
          (display "</p>")
          (get-output-string (current-output-port)))))

    (define get-replacement
      (lambda (key mode)
        (let ((r (if (string=? mode "encode")
                   (assoc key encode-replacements)
                   (assoc key decode-replacements))))
          (if r (car (cdr r)) key))))

    (define endecode
      (lambda (mode s)
        (if (not s)
          ""
          (letrec ((s-length (string-length s))
                   (looper
                     (lambda (i result)
                       (if (< i s-length)
                         (let ((key-length (if (and (string=? mode "decode")
                                                    (string=? (string-copy s i (+ i 1)) "%")
                                                    (> s-length (+ i 2)))
                                             3
                                             1)))
                           (looper (+ i key-length)
                                   (string-append result
                                                  (get-replacement
                                                    (string-copy s i (+ i key-length))
                                                    mode))))
                         result))))
            (looper 0 "")))))

    (define url-encode
      (lambda (str)
        (cond ((string? str) (endecode "encode" str))
              (else str))))

    (define url-decode
      (lambda (str)
        (cond ((string? str) (endecode "decode" str))
              (else str))))

    (define scgi-split-by-zero->list
      (lambda (source)
        (let ((result (list))
              (source-size (bytevector-length source)))
          (letrec ((looper
                     (lambda (index last-index key value)
                       (if (< index source-size)
                         (if (and key value)
                           (begin
                             (if (> (bytevector-length key) 0)
                               (set! result
                                 (append
                                   result
                                   (list (cons (string->symbol (utf8->string key))
                                               (if (= (bytevector-length value) 0)
                                                 ""
                                                 (utf8->string value)))))))
                             (looper index last-index #f #f))
                           (if (= (bytevector-u8-ref source index) 0)
                             (let ((slice (bytevector-copy source last-index index)))
                               (if (not key)
                                 (looper (+ index 1) (+ index 1) slice value)
                                 (looper (+ index 1) (+ index 1) key slice)))
                             (looper (+ index 1) last-index key value)))))))
            (looper 0 0 #f #f))
          result)))

    (define scgi-query-string->list
      (lambda (query-string)
        (let* ((query-bytevector (string->utf8 (string-append query-string "==")))
               (query-bytevector-size (bytevector-length query-bytevector))
               (u8-equal (bytevector-u8-ref (string->utf8 "=") 0))
               (u8-and (bytevector-u8-ref (string->utf8 "&") 0)))
          (letrec ((looper (lambda (index)
                             (if (< index query-bytevector-size)
                               (begin
                                 (if (or (= (bytevector-u8-ref query-bytevector index) u8-equal)
                                         (= (bytevector-u8-ref query-bytevector index) u8-and))
                                   (bytevector-u8-set! query-bytevector index 0))
                                 (looper (+ index 1)))))))
            (looper 0))
          (scgi-split-by-zero->list query-bytevector))))

    (define scgi-netstring->list
      (lambda (netstring)
        (let ((request (list)))
          (letrec ((get-request
                     (lambda (index)
                       (if (= (bytevector-u8-ref netstring index) 58)
                         (bytevector-copy netstring (+ index 1))
                         (get-request (+ index 1))))))
            (if (> (bytevector-length netstring) 0)
              (scgi-split-by-zero->list (get-request 0))
              (list))))))


    (define scgi-get-request-body
      (lambda (request-bytes content-length)
        (letrec ((looper
                   (lambda (index)
                     (if (and (> (bytevector-length request-bytes) 0)
                              (= (bytevector-u8-ref request-bytes index) 0)
                              (= (bytevector-u8-ref request-bytes (+ index 1)) 44))
                       (utf8->string (bytevector-copy request-bytes (+ index 2)))
                       (looper (- index 1))))))
          (looper (- (bytevector-length request-bytes) 1)))))

    (define read-all-from-socket
      (lambda (socket result)
        (let ((bytes (socket-recv socket 4000)))
          (if (or (eof-object? bytes)
                  (< (bytevector-length bytes) 4000))
            (bytevector-append result bytes)
            (read-all-from-socket socket (bytevector-append result bytes))))))

    (define scgi-handle
      (lambda (client-socket handler)
        (let* ((request-bytes (read-all-from-socket client-socket (bytevector))))
          (let*((request (scgi-netstring->list request-bytes))
                (request-method (if (not (null? request)) (cdr (assoc 'REQUEST_METHOD request)) ""))
                (request-uri (if (not (null? request)) (cdr (assoc 'REQUEST_URI request)) ""))
                (content-length (if (not (null? request)) (string->number (cdr (assoc 'CONTENT_LENGTH request))) 0))
                (body (url-decode (if (> content-length 0) (scgi-get-request-body request-bytes content-length) ""))))
            (set! request (append request (list (cons 'BODY body))))
            (call-with-current-continuation
              (lambda (k)
                (with-exception-handler
                  (lambda (ex)
                    (let ((error-message (make-html-error-message ex request body handler)))
                      (if debug?
                        (socket-send client-socket
                                     (string->utf8 error-message)))
                      (display error-message)
                      (k 'exception)))
                  (lambda ()
                    (for-each
                      (lambda (middleware-procedure)
                        (set! request (middleware-procedure request)))
                      request-middleware)
                    (let ((response (handler request)))
                      (for-each
                        (lambda (middleware-procedure)
                          (set! response (middleware-procedure response)))
                        response-middleware)
                      (socket-send client-socket
                                   (string->utf8 (if (string? response)
                                                   response
                                                   ""))))))))
            (if bridged?
              (scgi-handle client-socket handler)
              (socket-close client-socket))))))

    (define scgi-listen
      (lambda (socket handler)
        (scgi-handle (socket-accept socket) handler)
        (scgi-listen socket handler)))

    (define scgi-start
      (lambda (port handler)
        (let ((socket (make-server-socket port)))
          (scgi-listen socket handler))))

    (define scgi-start-debug
      (lambda (port handler)
        (set! debug? #t)
        (let ((socket (make-server-socket port)))
          (scgi-listen socket handler))))

    (define scgi-start-bridged
      (lambda (port handler)
        (set! bridged? #t)
        (let ((socket (make-server-socket port)))
          (scgi-listen socket handler))))

    (define scgi-start-debug-bridged
      (lambda (port handler)
        (set! bridged? #t)
        (set! debug? #t)
        (let ((socket (make-server-socket port)))
          (scgi-listen socket handler))))

    (define scgi-add-request-middleware
      (lambda (middleware-procedure)
        (set! request-middleware (append request-middleware (list middleware-procedure)))))

    (define scgi-add-response-middleware
      (lambda (middleware-procedure)
        (set! response-middleware (append response-middleware (list middleware-procedure)))))

    (define scgi-add-response-and-request-middleware
      (lambda (middleware-procedure)
        (set! request-middleware (append request-middleware (list middleware-procedure)))
        (set! response-middleware (append response-middleware (list middleware-procedure)))))))
