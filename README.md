Scheme library implementing [Simple Common Gateway Interface](https://python.ca/scgi/protocol.txt)

Should be portable, only uses R7RS features and [SRFI 106](https://srfi.schemers.org/srfi-106/srfi-106.html)

For FCGI see also the (fcgi-bridge)[https://git.sr.ht/~retropikzel/fcgi-bridge].

## Procedures

__scgi-start__ port main

Starts the scgi server. Blocks and waits for requests.
The first argument should be port scgi server should listen to, as string.
The second argument should be procedure that handles the request, it should
take one argument which is the request as alist. And it should return string
that is then sent to client.

__scgi-request-value-get__ request key

Helper procedure to get value out of a request.
Takes request and key as arguments, returns the value of the key or #f if not found.

## Simple example
### Scheme Server
    (import (scheme base)
            (scheme write)
            (retropikzel scgi v0-3-0 main))

    (scgi-start "3001"
        (lambda (request)
            (string-append "Content-type: text/html"
                           "\r\n"
                           "\r\n"
                           "Hello world")))

### HTTP Server

Using lighttpd might be the simplest to get started, install it and then
put this into file called lighttppd.conf in your project folder.

    server.document-root = "/your-project-path"
    server.errorlog = "/your-project-path/error.log"
    server.modules = ("mod_scgi")

    server.port = 3000
    scgi.debug = 1
    scgi.server = ("/" =>
                    (( "host" => "127.0.0.1",
                       "port" => 3001,
                       "check-local" => "disable")))

    mimetype.assign = (
                    ".html" => "text/html",
                    ".txt" => "text/plain",
                    ".jpg" => "image/jpeg",
                    ".png" => "image/png")

You can run this with command:

	lighttpd -D -f lighttpd.conf

and then open your browser to http://127.0.0.1:3000/
