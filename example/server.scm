(import (scheme base)
        (scheme write)
        (scheme load)
        (retropikzel scgi v0-3-0 main))

(define no-endpoint-handler
  (lambda (request)
    l
    (string-append "Content-type: text/html"
                   "\r\n"
                   "\r\n"
                   "No such endpoint")))

(define hello-handler
  (lambda (request)
    (string-append "Content-type: text/html"
                   "\r\n"
                   "\r\n"
                   "Hello world")))

(define request-middleware-write
  (lambda (request)
    (display "REQUEST:")
    (newline)
    (write request)
    (newline)
    (display "================")
    (newline)
    request))

(define response-middleware-write
  (lambda (response)
    (display "RESPONSE:")
    (newline)
    (write response)
    (newline)
    (display "================")
    (newline)
    response))

(define main
  (lambda (request)
    (let ((request-uri (cdr (assoc 'REQUEST_URI request))))
      (cond ((string=? request-uri "/hello")
             (hello-handler request))
            (else (no-endpoint-handler request))))))

(scgi-add-request-middleware request-middleware-write)
(scgi-add-response-middleware response-middleware-write)
(scgi-start-debug "3003" main)
